#!/bin/bash
# Trigger a schema reload in PostgREST.
# Read more about this at
# http://postgrest.org/en/v7.0.0/admin.html#schema-reloading.
set -euo pipefail
cd `dirname "$0"`/..

docker-compose kill -s SIGUSR1 postgrest
