-- Create our tables
CREATE SEQUENCE coop_id_seq;

CREATE TABLE coop(
	coop_id int NOT NULL
    DEFAULT nextval('coop_id_seq'::regclass),
  colour text NOT NULL,
  built timestamp(6),
  PRIMARY KEY (coop_id)
);

CREATE SEQUENCE chicken_id_seq;

CREATE TABLE chicken(
	chicken_id int NOT NULL
    DEFAULT nextval('chicken_id_seq'::regclass),
  name text,
  is_laying bool,
  coop int
    REFERENCES coop(coop_id),
  PRIMARY KEY (chicken_id)
);

-- Setup our role that PostgREST uses
CREATE ROLE web_anon NOLOGIN;
GRANT web_anon TO CURRENT_USER;
GRANT USAGE ON SCHEMA public TO web_anon;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE coop, chicken TO web_anon;
GRANT USAGE ON SEQUENCE coop_id_seq, chicken_id_seq TO web_anon;

-- Create a type to be used at the return type for our functions
CREATE TYPE chicken_aio_type AS (
  coop_id int,
  chicken_ids int[]
);

-- Create a function that works with JSON params
CREATE OR REPLACE FUNCTION chicken_aio(coop json, chickens json[])
RETURNS chicken_aio_type AS $$
DECLARE
  new_coop_id int;
  x json;
  new_chicken_ids int[];
  newest_chicken_id int;
BEGIN
  RAISE WARNING 'coop %, chickens %', coop, chickens;
  INSERT INTO coop (colour, built) VALUES(
    (coop->>'colour'),
    (coop->>'built')::timestamp(6)
  ) RETURNING coop_id INTO new_coop_id;
  FOREACH x IN ARRAY chickens LOOP
    RAISE WARNING 'loop item %', x;
    INSERT INTO chicken (name, is_laying, coop) VALUES(
      x->>'name',
      (x->>'is_laying')::bool,
      new_coop_id
    ) RETURNING chicken_id INTO newest_chicken_id;
    new_chicken_ids := new_chicken_ids || newest_chicken_id;
  END LOOP;
  RETURN (new_coop_id, new_chicken_ids);
END
$$ LANGUAGE plpgsql;

-- Test the function out
SELECT chicken_aio(
  '{"colour":"white","built":"2020-01-30"}'::json,
  '{"{\"name\":\"Tofu\",\"is_laying\":true}","{\"name\":\"Aztec\",\"is_laying\":false}"}'::json[]
);

-- Create a function that does the same as the previous one but uses row types
-- from our tables rather than just plain JSON.
CREATE OR REPLACE FUNCTION chicken_aio_rt(coop coop, chickens chicken[]) -- different param types
RETURNS chicken_aio_type AS $$
DECLARE
  new_coop_id int;
  x chicken; -- different var type
  new_chicken_ids int[];
  newest_chicken_id int;
BEGIN
  RAISE WARNING 'coop %, chickens %', coop, chickens;
  INSERT INTO coop (colour, built) VALUES(
    coop.colour, -- different way of accessing fields
    coop.built
  ) RETURNING coop_id INTO new_coop_id;
  FOREACH x IN ARRAY chickens LOOP
    RAISE WARNING 'loop item %', x;
    INSERT INTO chicken (name, is_laying, coop) VALUES(
      x.name,
      x.is_laying,
      new_coop_id
    ) RETURNING chicken_id INTO newest_chicken_id;
    new_chicken_ids := new_chicken_ids || newest_chicken_id;
  END LOOP;
  RETURN (new_coop_id, new_chicken_ids);
END
$$ LANGUAGE plpgsql;

-- Test the function out
SELECT chicken_aio_rt(
  ROW(NULL,'red','2020-02-02')::coop,
  ARRAY[ROW(NULL,'Bertie',true,NULL),ROW(NULL,'Sterling',false,NULL)]::chicken[]
);

