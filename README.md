# What is this
[PostgREST](http://postgrest.org/) is an awesome tool for turning a PostgreSQL
database into a RESTful HTTP API.

One thing it can't currently handle is [relational
inserts](https://github.com/PostgREST/postgrest/issues/818#issuecomment-409981816).
That is, being able to create a parent record and some linked (by foreign key)
child records all at the same time.

One way to work around this is with a `FUNCTION` in the database that gets
exposed as an RPC call. A single HTTP call can then create the parent and all
the children. This repository is a demo of how that can be done.

It's not perfect because the generated OpenAPI document for the RPC endpoints is
missing the type information so it just thinks the type is `"string"`. There's
some discussion around this "string is the default type" stuff
[here](https://github.com/PostgREST/postgrest/issues/1377#issuecomment-525077875).

# Running the demo

  1. make sure you have Docker and docker-compose installed
  1. clone this repo
  1. start the docker-compose stack
      ```bash
      docker-compose up -d
      ```
  1. the stack should start within a few seconds, then you can view the Swagger
     UI for the API at [http://localhost:38080/](http://localhost:38080/)
  1. to test out either of the RPC endpoints, you can use the POST body:
      ```json
      {
        "coop": {"colour":"blue","built":"2020-03-25"},
        "chickens":[
          {"name":"Disco","is_laying":false},
          {"name":"Chops","is_laying":true}
        ]
      }
      ```
  1. have a poke around in the database with: `./helper-scripts/psql.sh`
  1. clean up everything with
      ```bash
      docker-compose down --volumes
      ```

# Why two functions?
One function uses `json` for the params, so there's no type safety.

The second uses typed params from the tables it works with. When you issue SQL
queries for this second form, you get good type safety even if the way to call
it (see in `schema.sql`) is clunky. Unfortunately the type safety doesn't carry
over to PostgREST quite as well.

For both approaches, if you fail any DB level constraints (like `NOT NULL`) then
the RPC call will error out but the error messages are very nice for the end
user.
